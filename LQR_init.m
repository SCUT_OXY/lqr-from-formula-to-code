%% 建立二阶系统
%二阶系统传递函数
b0 = 2;
a0 = 0;
a1 = 10;
num = [b0];%分子
den = [1 a1 a0];%分母
G = tf(num,den)

%转化为状态空间矩阵形式
[A,B,C,D] = tf2ss(num,den);
%也可以用能控标准I型
A = [0 1
     -a0 -a1];
B = [0
     b0];
C = [1 0];
D = 0;
% 设置QR矩阵
Q = [1 0
     0 1];
R = 1;
K = lqr(A,B,Q,R)














%% 对比Q矩阵对控制的影响
% 计算LQR参数
Q = diag([1,1]);
R = diag(1);
K1 = lqr(A,B,Q,R)
% 第二组LQR参数（用来对比QR矩阵对性能的影响）
Q = diag([1,1]);
R = diag(10);
K2 = lqr(A,B,Q,R)
%% 加入积分跟踪之后
% A = [0 1 0
%      -a0 -a1 0
%      -1 0 0];
% B = [0
%      b0
%      0];
% C = [1 0 0];
% D = 0;
% % 计算LQR参数
% Q = diag([1,1,100]);
% R = diag(1);
% K3 = lqr(A,B,Q,R)